resource "azurerm_virtual_network" "vnet" {
    name                = "${var.myResourceGroup}vnet"
    address_space       = ["10.0.0.0/16"]
    location            = "${var.myLocation}"
    resource_group_name = "${var.myResourceGroup}"
}

resource "azurerm_subnet" "subnet1" {
    name                 = "${var.myResourceGroup}subnet1"
    resource_group_name  = "${var.myResourceGroup}"
    virtual_network_name = "${azurerm_virtual_network.vnet.name}"
    address_prefix       = "10.0.1.0/24"
}

resource "azurerm_subnet" "subnet2" {
    name                 = "${var.myResourceGroup}subnet2"
    resource_group_name  = "${var.myResourceGroup}"
    virtual_network_name = "${azurerm_virtual_network.vnet.name}"
    address_prefix       = "10.0.2.0/24"
}

resource "azurerm_network_security_group" "sgssh" {
    name                = "${var.myResourceGroup}ssh"
    location            = "${var.myLocation}"
    resource_group_name = "${var.myResourceGroup}"
    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}

resource "azurerm_network_security_group" "sghttp" {
    name                = "${var.myResourceGroup}http"
    location            = "${var.myLocation}"
    resource_group_name = "${var.myResourceGroup}"
    security_rule {
        name                       = "HTTP"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
    security_rule {
        name                       = "HTTPS"
        priority                   = 1002
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "443"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}

terraform {
required_version = "~> 0.11.8"
  backend "azurerm" {
    storage_account_name = "your storage account"
    container_name = "your storage account container name"
    key = "your tfstates key"
    access_key = "your storage account access key"
  }
}
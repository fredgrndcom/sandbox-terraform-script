provider "azurerm" {
  version = "~> 1.14"
}

provider "random" {
  version = "~> 2.0"
}

provider "ansible" {
  version = "~> 0.0.4"
}

resource "azurerm_resource_group" "rg" {
    name     = "${var.myResourceGroup}"
    location = "${var.myLocation}"
}

resource "random_id" "randomId" {
    keepers = {
        resource_group = "${var.myResourceGroup}"
    }
    byte_length = 8
}

resource "azurerm_storage_account" "sa" {
    name                        = "diag${random_id.randomId.hex}"
    resource_group_name         = "${var.myResourceGroup}"
    location                    = "${var.myLocation}"
    account_tier                = "Standard"
    account_replication_type    = "LRS"
}

resource "azurerm_public_ip" "pip" {
    count = "${var.nb_instances}"
    name = "${var.modResourceGroup}-pip-${count.index}"
    location = "${var.modLocation}"
    resource_group_name = "${var.modResourceGroup}"
    public_ip_address_allocation = "dynamic"
}

resource "azurerm_network_interface" "nic" {
    count = "${var.nb_instances}"
    name = "${var.modResourceGroup}-nic-${count.index}"
    location = "${var.modLocation}"
    resource_group_name = "${var.modResourceGroup}"
    network_security_group_id = "${var.modNSG}"

    ip_configuration {
        name = "${var.modResourceGroup}-nic-${count.index}"
        subnet_id = "${var.modSubnet}"
        private_ip_address_allocation = "dynamic"
        public_ip_address_id = "${length(azurerm_public_ip.pip.*.id) > 0 ? element(concat(azurerm_public_ip.pip.*.id, list("")), count.index) : ""}"
    }
}

resource "ansible_host" "vm" {
    depends_on = ["azurerm_virtual_machine.vm"]
    count = "${var.nb_instances}"
    inventory_hostname = "${element(azurerm_public_ip.pip.*.ip_address, count.index)}"
    groups = ["core","web"]
    vars {
        ansible_user = "${var.modUser}"
    } 
}

resource "azurerm_virtual_machine" "vm" {
    count = "${var.nb_instances}"
    name = "${var.modResourceGroup}-${count.index}"
    location = "${var.modLocation}"
    resource_group_name = "${var.modResourceGroup}"
    network_interface_ids = ["${element(azurerm_network_interface.nic.*.id, count.index)}"]
    vm_size = "${var.modVmSize}"

    storage_os_disk {
        name = "${var.modResourceGroup}vmOsDisk-${count.index}"
        caching = "ReadWrite"
        create_option = "FromImage"
        managed_disk_type = "Standard_LRS"
    }

    storage_image_reference {
        publisher = "${var.modOsPublisher}"
        offer = "${var.modOsOffer}"
        sku = "${var.modOsSku}"
        version = "${var.modOsVersion}"
    }

    os_profile {
        computer_name = "${var.modResourceGroup}-${count.index}"
        admin_username = "${var.modUser}"
    }

    os_profile_linux_config {
        disable_password_authentication = true
        ssh_keys {
            path = "/home/${var.modUser}/.ssh/authorized_keys"
            key_data = "${var.modSSHKey}"
        }
    }

    boot_diagnostics {
        enabled = "true"
        storage_uri = "${var.modBootStorageUri}"
    }
}
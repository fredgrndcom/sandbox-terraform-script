variable "modResourceGroup" {}

variable "modLocation" {}

variable "modUser" {}

variable "modSSHKey" {}

variable "modOsPublisher" {}

variable "modOsOffer" {}

variable "modOsSku" {}

variable "modOsVersion" {}

variable "modVmSize" {}

variable "modNSG" {}

variable "modSubnet" {}

variable "modBootStorageUri" {}

variable "nb_instances" {
  description = "Specify the number of vm instances"
  default     = "1"
}
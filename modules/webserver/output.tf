output "public_ip" {
  description = "Server Public IP."
  value = "${azurerm_public_ip.pip.*.ip_address}"
}

output "private_ip" {
  description = "Server Private IP"
  value = "${azurerm_network_interface.nic.*.private_ip_address}"
}

output "hostname" {
  description = "Server hostname"
  value = "${azurerm_virtual_machine.vm.*.name}"
}

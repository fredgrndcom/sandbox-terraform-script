output "Public IP"{
  value = "${module.front.public_ip}"
}

output "Hostname" {
  value = "${module.front.hostname}"
}

output "Private IP" {
  value = "${module.front.private_ip}"
}

output "Application Gateway Public IP" {
  value = "${azurerm_public_ip.appgwpip.ip_address}"
}

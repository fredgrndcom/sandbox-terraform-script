resource "azurerm_public_ip" "appgwpip" {
  name                         = "${var.myResourceGroup}-pip-appgw"
  location                     = "${var.myLocation}"
  resource_group_name          = "${var.myResourceGroup}"
  public_ip_address_allocation = "dynamic"
}

resource "azurerm_application_gateway" "appgw" {
  name                = "${var.myResourceGroup}-appgw"
  resource_group_name = "${var.myResourceGroup}"
  location            = "${var.myLocation}"

  sku {
    name           = "Standard_Small"
    tier           = "Standard"
    capacity       = 2
  }

  gateway_ip_configuration {
    name         = "${var.myResourceGroup}-nic-appgw"
    subnet_id    = "${azurerm_virtual_network.vnet.id}/subnets/${azurerm_subnet.subnet2.name}"
  }

  frontend_port {
    name         = "${azurerm_virtual_network.vnet.name}-feport"
    port         = 80
  }

  frontend_ip_configuration {
    name         = "${azurerm_virtual_network.vnet.name}-feip"
    public_ip_address_id = "${azurerm_public_ip.appgwpip.id}"
  }

  backend_address_pool {
    name = "${azurerm_virtual_network.vnet.name}-beap"
    ip_address_list = ["${module.front.private_ip}"]
  }

  backend_http_settings {
    name                  = "${azurerm_virtual_network.vnet.name}-be-htst"
    cookie_based_affinity = "Disabled"
    port                  = 80
    protocol              = "Http"
    request_timeout       = 1
  }

  http_listener {
    name                            = "${azurerm_virtual_network.vnet.name}-httplstn"
    frontend_ip_configuration_name  = "${azurerm_virtual_network.vnet.name}-feip"
    frontend_port_name              = "${azurerm_virtual_network.vnet.name}-feport"
    protocol                        = "Http"
  }

  request_routing_rule {
    name                       = "${azurerm_virtual_network.vnet.name}-rqrt"
    rule_type                  = "Basic"
    http_listener_name         = "${azurerm_virtual_network.vnet.name}-httplstn"
    backend_address_pool_name  = "${azurerm_virtual_network.vnet.name}-beap"
    backend_http_settings_name = "${azurerm_virtual_network.vnet.name}-be-htst"
  }
}
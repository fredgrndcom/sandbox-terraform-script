variable "myResourceGroup" {
  default = "sandbox01"
}

variable "myLocation" {
  default = "West Europe"
}

variable "myInstanceCount" {
  default = "2"
}


variable "myUser" {
  default = "azadmin"
}



variable "mySSHKey" {
  default = ""
}

variable "myOsPublisher" {
  default = "OpenLogic"
}

variable "myOsOffer" {
  default = "CentOS"
}

variable "myOsSku" {
  default = "6.9"
}

variable "myOsVersion" {
  default = "latest"
}

variable "myVmSize" {
  default = "Standard_B1s"
}
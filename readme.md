# Sandbox VM

Simple Terraform script to deploy Linux Box in Azure:

- 1 Ressource Group
- 1 Vnet
- 1 Subnet
- 2 Security groups SSH and HTTP
- x VM module whith a public IP
- Application Gateway for HTTP

Ansible playbook

- Disable Selinux
- Intall HTTPD with mod_ssl

Before start

- *This script use [ansible terraform third party plugin](https://github.com/nbering/terraform-provider-ansible), you need to install it before play*
- *bin/terraform.py was provided by [nbering](https://github.com/nbering/terraform-inventory)*

## Howto

### Export Azure credentials

Simply export Azure Principal Accout env variables like :

```bash

export ARM_CLIENT_ID=<your client ID>
export ARM_CLIENT_SECRET=<your client secret>
export ARM_SUBSCRIPTION_ID=<your subscription ID>
export ARM_TENANT_ID=<your tenant ID>

```

### Storage Account for TFSTATES storage

Create if don't have one by following this documentation : [Terraform remote state with Microsoft Azure](http://www.blueshift.dk/blog/terraform-remote-state-azure)

### Configure your storage key

In backend.tf file change

```access_key = "your storage account access key"```

### Change variables

By default this script deploy a Centos 6.9 linux box, you can change parameters in the variales.tf file.

### Deploy and destroy Vm

First you need to initialize Terraform

```bash

terraform init

```

See planed change

```bash

terraform plan

```

Apply

```bash

terraform apply

```

Destory vm

```bash

terraform destroy

```